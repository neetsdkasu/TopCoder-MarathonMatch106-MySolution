@echo off
if "%~1"=="" goto defaultseed
java -Xms512M -jar tester.jar -exec "java -Xms512M -cp classes Main" -debug -seed %*
@GOTO finally
:defaultseed
java -Xms512M -jar tester.jar -exec "java -Xms512M -cp classes Main" -debug -seed 1
@GOTO finally
:finally
