import java.io.*;
import java.util.*;

public class StainedGlass {

    static long TIME_LIMIT = 19_000_000_000L;

    long startTime, limitTime;
    Random rand = new Random(198319831983L);

    private static final int VSize = 100;

    int QSize = 5;

    int H, W, N;
    int[] pixels;
    int[][] flag, nearest;
    int[][][] field;
    IntList[][] colors;

    public int[] create(int H, int[] pixels, int N) {
        startTime = System.nanoTime();
        limitTime = startTime + TIME_LIMIT;

        this.H = H;
        this.W = pixels.length / H;
        this.N = N;
        this.pixels = pixels;

        System.err.printf("H: %d, W: %d, N: %d%n", H, W, N);

        boolean quantiFlag = false;
        {
            HashSet<Integer> qhs = new HashSet<>();
            for (int p : pixels) {
                qhs.add(p);
                if (qhs.size() > N / 2) {
                    quantiFlag = true;
                    break;
                }
            }
            qhs.clear();
            qhs = null;
        }

        field = new int[VSize][VSize][3];
        nearest = new int[VSize][VSize];

        colors = new IntList[N][3];
        for (int i = 0; i < N; i++) {
            for (int k = 0; k < 3; k++) {
                colors[i][k] = new IntList(1000);
            }
        }

        long time0 = System.nanoTime();
        Result ret = generate(quantiFlag, false);
        long time1 = System.nanoTime();
        long interval = time1 - time0;
        int selr = QSize + 1;

    outerloop:
        for (int cycle = 0; cycle < 3; cycle++) {
            QSize = 2;

            for (int i = 0; i < 10; i++) {
                QSize += 1;
                time0 = System.nanoTime();
                if (time0 + interval > limitTime) {
                    System.err.printf("cycle: %d, i: %d, interval: %d ms%n", cycle, i, interval / 1_000_000L);
                    break outerloop;
                }
                Result tmp = generate(quantiFlag, false);
                time1 = System.nanoTime();
                interval = Math.max(interval, time1 - time0);
                if (tmp.score < ret.score) {
                    ret = tmp;
                    selr = QSize + 1 + cycle * 1000;
                }
            }
        }

        System.err.printf("selr: %d, qf: %b, sc: %f, cc: %d, nn: %d%n",
            selr, quantiFlag, ret.score, ret.colorCount, ret.points.length / 3);

        return ret.points;
    }

    private Result generate(boolean quantiFlag, boolean compressFlag) {

        IntList retL = new IntList(N * 3);
        flag = new int[VSize][VSize];

        for (int i = 0; i < N; i++) {
            int x = rand.nextInt(VSize);
            int y = rand.nextInt(VSize);
            while (flag[y][x] > 0) {
                x = rand.nextInt(VSize);
                y = rand.nextInt(VSize);
            }
            flag[y][x] = 1;
            retL.add(y);
            retL.add(x);
            retL.add(0);
        }
        int[] ret = retL.toArray();
        retL.clear();

        for (int i = 0; i < N; i++) {
            for (int k = 0; k < 3; k++) {
                colors[i][k].clear();
            }
        }

        IntList es1 = new IntList((H / VSize + 1) * (W / VSize + 1));
        for (int row = 0; row < VSize; row++) {
            int dh = H / VSize;
            int y = row * dh;
            if (row < H % VSize) {
                y += row;
                dh += 1;
            } else {
                y += H % VSize;
            }
            for (int col = 0; col < VSize; col++) {
                int dw = W / VSize;
                int x = col * dw;
                if (col < W % VSize) {
                    x += col;
                    dw += 1;
                } else {
                    x += W % VSize;
                }
                int c = 0;
                IntList es = es1;
                for (int k = 0; k < 3; k++) {
                    es.clear();
                    for (int dy = 0; dy < dh; dy++) {
                        for (int dx = 0; dx < dw; dx++) {
                            int e = pixels[(y + dy) * W + (x + dx)];
                            e = factor(e, k);
                            if (quantiFlag) { e = quantization(e); }
                            es.add(e);
                        }
                    }
                    es.sort();
                    int f = es.get(es.size() / 2);
                    c |= f << (k << 3);
                    field[row][col][k] = f;
                }
                int minDist = Integer.MAX_VALUE;
                int seli = 0;
                for (int i = 0; i < N; i++) {
                    int dr = row - ret[i * 3];
                    int dc = col - ret[i * 3 + 1];
                    int dist = dr * dr + dc * dc;
                    if (dist < minDist) {
                        seli = i;
                        minDist = dist;
                    }
                }
                nearest[row][col] = seli;
                for (int k = 0; k < 3; k++) {
                    Integer e = factor(c, k);
                    colors[seli][k].add(e);
                }
            }
        }
        es1 = null;

        for (int i = 0; i < N; i++) {
            int c = 0;
            for (int k = 0; k < 3; k++) {
                IntList es = colors[i][k];
                es.sort();
                c |= es.get(es.size() / 2) << (k << 3);
            }
            ret[i * 3 + 2] = c;
        }

        int[] tmp = ret;

        IntList ptos = new IntList(2000);
        IntList pfrs = new IntList(2000);

        for (int cycle = 0; cycle < 15000; cycle++) {
            int ip = cycle % N;
            int dy = rand.nextInt(5) - 2;
            int dx = rand.nextInt(5) - 2;
            int ny = Math.max(0, Math.min(VSize - 1, tmp[ip * 3] + dy));
            int nx = Math.max(0, Math.min(VSize - 1, tmp[ip * 3 + 1] + dx));
            int sc = 0;
            int nc = tmp[ip * 3 + 2];
            ptos.clear();
            pfrs.clear();
            if (nc < 0) {
                int cl;
                if (flag[tmp[ip * 3]][tmp[ip * 3 + 1]] == 1 && rand.nextInt(100) < 10) {
                    ny = tmp[ip * 3];
                    nx = tmp[ip * 3 + 1];
                    cl = ~nc;
                } else if (flag[ny][nx] == 0 && rand.nextInt(100) < 10) {
                    cl = ~nc;
                } else {
                    do {
                        ny = rand.nextInt(VSize);
                        nx = rand.nextInt(VSize);
                    } while (flag[ny][nx] > 0);
                    cl = (field[ny][nx][2] << 16) | (field[ny][nx][1] << 8) | field[ny][nx][0];
                }
                tmp[ip * 3 + 2] = cl;
                sc = calcAppearScore(tmp, ip, ny, nx, pfrs);
                tmp[ip * 3 + 2] = nc;
                nc = cl;
            } else if (flag[ny][nx] > 0) {
                sc = calcDisappearScore(tmp, ip, ptos);
                nc = ~nc;
            } else {
                if (ny == tmp[ip * 3] && nx == tmp[ip * 3 + 1]) {
                    for (int ty = Math.max(0, tmp[ip * 3] - 2); ty < Math.min(VSize - 1, tmp[ip * 3] + 2); ty++) {
                        for (int tx = Math.max(0, tmp[ip * 3 + 1] - 2); tx < Math.min(VSize - 1, tmp[ip * 3 + 1] + 2); tx++) {
                            if (ny == ty && tx == nx) { continue; }
                            if (ny == tmp[ip * 3] && nx == tmp[ip * 3 + 1]) {
                                ny = ty;
                                nx = tx;
                            } else if (rand.nextInt(100) < 50) {
                                ny = ty;
                                nx = tx;
                            }
                        }
                    }
                }
                sc = calcMoveScore(tmp, ip, ny, nx, ptos, pfrs);
            }

            if (sc > 0) { continue; }

            int oy = tmp[ip * 3];
            int ox = tmp[ip * 3 + 1];
            flag[oy][ox] -= 1;
            flag[ny][nx] += 1;
            tmp[ip * 3] = ny;
            tmp[ip * 3 + 1] = nx;
            tmp[ip * 3 + 2] = nc;

            for (int i = 0; i < ptos.size(); i += 3) {
                int row = ptos.get(i);
                int col = ptos.get(i + 1);
                int sel = ptos.get(i + 2);
                nearest[row][col] = sel;
            }

            for (int i = 0; i < pfrs.size(); i += 3) {
                int row = pfrs.get(i);
                int col = pfrs.get(i + 1);
                // int old = pfrs.get(i + 2);
                nearest[row][col] = ip;
            }

        }

        ret = tmp;

        int[] idx = new int[N];
        retL.clear();
        for (int i = 0; i < N; i++) {
            if (ret[i * 3 + 2] < 0) { continue; }
            int y = ret[i * 3] * (H / VSize);
            int x = ret[i * 3 + 1] * (W / VSize);
            if (ret[i * 3] < H % VSize) {
                y += ret[i * 3];
            } else {
                y += H % VSize;
            }
            if (ret[i * 3 + 1] < W % VSize) {
                x += ret[i * 3 + 1];
            } else {
                x += W % VSize;
            }
            idx[i] = retL.size() / 3;
            retL.add(y + (H / VSize) / 2);
            retL.add(x + (W / VSize) / 2);
            retL.add(ret[i * 3 + 2]);
        }
        ret = retL.toArray();

        int nn = ret.length / 3;
        IntList[] ps = new IntList[N];
        for (int i = 0; i < N; i++) {
            ps[i] = new IntList(1000);
        }

        for (int row = 0; row < VSize; row++) {
            int dh = H / VSize;
            int y = row * dh;
            if (row < H % VSize) {
                y += row;
                dh += 1;
            } else {
                y += H % VSize;
            }
            for (int col = 0; col < VSize; col++) {
                int dw = W / VSize;
                int x = col * dw;
                if (col < W % VSize) {
                    x += col;
                    dw += 1;
                } else {
                    x += W % VSize;
                }
                HashSet<Integer> hs = new HashSet<>();
                for (int tr = Math.max(0, row - 3); tr < Math.min(VSize, row + 4); tr++) {
                    for (int tc = Math.max(0, col - 3); tc < Math.min(VSize, col + 4); tc++) {
                        hs.add(idx[nearest[tr][tc]]);
                    }
                }
                Integer[] ts = hs.toArray(new Integer[0]);
                for (int ty = y; ty < y + dh; ty++) {
                    for (int tx = x; tx < x + dw; tx++) {
                        int minDist = Integer.MAX_VALUE;
                        int seli = 0;
                        for (int i : ts) {
                            int dist = distance2(ty, tx, ret[i * 3], ret[i * 3 + 1]);
                            if (dist < minDist) {
                                minDist = dist;
                                seli = i;
                            }
                        }
                        ps[seli].add(pixels[ty * W + tx]);
                    }
                }
            }
        }

        for (int i = 0; i < nn; i++) {
            IntList psi = ps[i];
            if (psi.isEmpty()) { continue; }
            int c = 0;
            int[] es = new int[psi.size()];
            for (int k = 0; k < 3; k++) {
                for (int j = 0; j < psi.size(); j++) {
                    int e = psi.get(j);
                    int f = factor(e, k);
                    if (quantiFlag) { f = quantization(f); }
                    es[j] = f;
                }
                Arrays.sort(es);
                int ef = es[es.length / 2];
                c |= ef << (k << 3);
            }
            ret[i * 3 + 2] = c;
        }

        if (compressFlag) {
            for (int i = 0; i < nn; i++) {
                int c = ret[i * 3 + 2];
                int selj = i;
                int seld = Integer.MAX_VALUE;
                for (int j = 0; j < nn; j++) {
                    if (i == j) { continue; }
                    int e = ret[j * 3 + 2];
                    int d = 0;
                    for (int k = 0; k < 3; k++) {
                        int f = factor(e, k) - factor(c, k);
                        d += f * f;
                    }
                    if (d < seld) {
                        selj = j;
                        seld = d;
                    }
                }
                ret[i * 3 + 2] = ret[selj * 3 + 2];
            }
        }

        HashMap<Integer, IntList> shs = new HashMap<>();
        for (int i = 0; i < nn; i++) {
            IntList psi = ps[i];
            if (psi.isEmpty()) { continue; }
            Integer c = ret[i * 3 + 2];
            IntList cs = shs.get(c);
            if (cs == null) {
                cs = new IntList(psi);
                shs.put(c, cs);
            } else {
                cs.addAll(psi);
            }
        }

        int score = 0;

        HashMap<Integer, Integer> newColors = new HashMap<>();
        for (Map.Entry<Integer, IntList> me : shs.entrySet()) {
            IntList vs = me.getValue();
            int[] es = new int[vs.size()];
            int c = 0;
            for (int k = 0; k < 3; k ++) {
                for (int i = 0; i < es.length; i++) {
                    es[i] = factor(vs.get(i), k);
                }
                Arrays.sort(es);
                int ef = es[es.length / 2];
                c |= ef << (k << 3);
                for (int e : es) {
                    score += Math.abs(e - ef);
                }
            }
            newColors.put(me.getKey(), c);
        }

        for (int i = 0; i < nn; i++) {
            int or = ret[i * 3 + 2];
            Integer cl = newColors.get(or);
            if (cl == null) {
                int f = Integer.MAX_VALUE;
                for (Integer e : newColors.values()) {
                    int t = 0;
                    for (int k = 0; k < 3; k++) {
                        int d = factor(e, k) - factor(or, k);
                        t += d * d;
                    }
                    if (t < f) {
                        f = t;
                        cl = e;
                    }
                }
                if (cl == null) { continue; }
            }
            ret[i * 3 + 2] = cl;
        }

        double dScore = (double)score * Math.pow(1.0 + (double)shs.size() / (double)N, 2);

        return new Result(ret, dScore, shs.size());
    }

    private int quantization(int f) {
        return ((QSize * f + 128) / 255) * 255 / QSize;
    }

    private int factor(int c, int k) {
        return (c >> (k << 3)) & 0xFF;
    }

    private int distance2(int y1, int x1, int y2, int x2) {
        int dy = y1 - y2;
        int dx = x1 - x2;
        return dy * dy + dx * dx;
    }

    private int[] toIntArray(List<Integer> xs) {
        int[] ret = new int[xs.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = xs.get(i);
        }
        return ret;
    }

    private int calcMoveScore(int[] tmp, int ip, int ny, int nx, IntList ptos, IntList pfrs) {

        int sc = 0;

        for (int row = 0; row < VSize; row++) {
            for (int col = 0; col < VSize; col++) {
                int bp = nearest[row][col];
                if (ip == bp) {
                    int minDist = Integer.MAX_VALUE;
                    int selj = 0;
                    for (int j = 0; j < N; j++) {
                        if (tmp[j * 3 + 2] < 0) { continue; }
                        int dist;
                        if (j == ip) {
                            dist = distance2(row, col, ny, nx);
                        } else {
                            dist = distance2(row, col, tmp[j * 3], tmp[j * 3 + 1]);
                        }
                        if (dist < minDist) {
                            minDist = dist;
                            selj = j;
                        }
                    }
                    if(selj != ip) {
                        ptos.add(row);
                        ptos.add(col);
                        ptos.add(selj);
                        for (int k = 0; k < 3; k++) {
                            int e = field[row][col][k];
                            sc += Math.abs(e - factor(tmp[selj * 3 + 2], k));
                            sc -= Math.abs(e - factor(tmp[ip * 3 + 2], k));
                        }
                    }
                } else {
                    int bDist = distance2(row, col, tmp[bp * 3], tmp[bp * 3 + 1]);
                    int nDist = distance2(row, col, ny, nx);
                    if (nDist < bDist) {
                        pfrs.add(row);
                        pfrs.add(col);
                        pfrs.add(bp);
                        for (int k = 0; k < 3; k++) {
                            int e = field[row][col][k];
                            sc -= Math.abs(e - factor(tmp[bp * 3 + 2], k));
                            sc += Math.abs(e - factor(tmp[ip * 3 + 2], k));
                        }
                    }
                }
            }
        }

        return sc;
    }


    private int calcDisappearScore(int[] tmp, int ip, IntList ptos) {
        int sc = 0;

        for (int row = 0; row < VSize; row++) {
            for (int col = 0; col < VSize; col++) {
                int bp = nearest[row][col];
                if (ip != bp) { continue; }
                int minDist = Integer.MAX_VALUE;
                int selj = -1;
                for (int j = 0; j < N; j++) {
                    if (j == ip || tmp[j * 3 + 2] < 0) { continue; }
                    int dist = distance2(row, col, tmp[j * 3], tmp[j * 3 + 1]);
                    if (dist < minDist) {
                        minDist = dist;
                        selj = j;
                    }
                }
                if (selj < 0) {
                    return Integer.MAX_VALUE;
                }
                if (selj != ip) {
                    ptos.add(row);
                    ptos.add(col);
                    ptos.add(selj);
                    for (int k = 0; k < 3; k++) {
                        int e = field[row][col][k];
                        sc += Math.abs(e - factor(tmp[selj * 3 + 2], k));
                        sc -= Math.abs(e - factor(tmp[ip * 3 + 2], k));
                    }
                }
            }
        }

        return sc;
    }

    private int calcAppearScore(int[] tmp, int ip, int ny, int nx, IntList pfrs) {

        int sc = 0;

        for (int row = 0; row < VSize; row++) {
            for (int col = 0; col < VSize; col++) {
                int bp = nearest[row][col];
                int bDist = distance2(row, col, tmp[bp * 3], tmp[bp * 3 + 1]);
                int nDist = distance2(row, col, ny, nx);
                if (nDist < bDist) {
                    pfrs.add(row);
                    pfrs.add(col);
                    pfrs.add(bp);
                    for (int k = 0; k < 3; k++) {
                        int e = field[row][col][k];
                        sc -= Math.abs(e - factor(tmp[bp * 3 + 2], k));
                        sc += Math.abs(e - factor(tmp[ip * 3 + 2], k));
                    }
                }
            }
        }

        return sc;
    }

}

class Result {
    final int[] points;
    final double score;
    final int colorCount;
    Result(int[] p, double s, int c) {
        points = p;
        score = s;
        colorCount = c;
    }
}



class IntList {
    int[] buf;
    int alloc;
    int index = 0;
    IntList(int cap, int a) {
        buf = new int[cap];
        alloc = a;
    }
    IntList(int cap) {
        this(cap, 1000);
    }
    IntList() {
        this(1000, 1000);
    }
    IntList(IntList other) {
        this(other.buf.length, other.alloc);
        System.arraycopy(other.buf, 0, buf, 0, other.index);
        index = other.index;
    }
    void add(int value) {
        if (index >= buf.length) {
            buf = Arrays.copyOf(buf, buf.length + alloc);
        }
        buf[index] = value;
        index++;
    }
    void addAll(IntList other) {
        if (index + other.index >= buf.length) {
            buf = Arrays.copyOf(buf, index + other.index);
        }
        System.arraycopy(other.buf, 0, buf, index, other.index);
        index += other.index;
    }
    int size() {
        return index;
    }
    int[] toArray() {
        return Arrays.copyOf(buf, index);
    }
    void release() {
        buf = null;
    }
    int get(int i) {
        return buf[i];
    }
    void clear() {
        index = 0;
    }
    boolean isEmpty() {
        return index == 0;
    }
    void sort() {
        Arrays.sort(buf, 0, index);
    }
}