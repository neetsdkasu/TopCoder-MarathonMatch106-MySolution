import java.io.*;
import java.util.*;

public class Main {

    static Stopwatch sw = new Stopwatch();

    static int callCreate(BufferedReader in, StainedGlass stainedGlass) throws Exception {

        int H = Integer.parseInt(in.readLine());
        int _pixelsSize = Integer.parseInt(in.readLine());
        int[] pixels = new int[_pixelsSize];
        for (int _idx = 0; _idx < _pixelsSize; _idx++) {
            pixels[_idx] = Integer.parseInt(in.readLine());
        }
        int N = Integer.parseInt(in.readLine());

        sw.start();
        int[] _result = stainedGlass.create(H, pixels, N);
        sw.stop();
        System.err.printf("time: %d ms%n", sw.getMillis());
        System.err.flush();

        System.out.println(_result.length);
        for (int _it : _result) {
            System.out.println(_it);
        }
        System.out.flush();

        return 0;
    }

    public static void main(String[] args) throws Exception {

        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        sw.start();
        StainedGlass stainedGlass = new StainedGlass();
        sw.stop();

        StainedGlass.TIME_LIMIT *= 6L;

        // do edit codes if you need

        callCreate(in, stainedGlass);

    }

}

class Stopwatch {
    long total = 0L;
    long startTime = 0L;
    Stopwatch() {}
    void start() { startTime = System.nanoTime(); }
    void stop() { long endTime = System.nanoTime(); total += endTime - startTime; }
    long getMillis() { return total / 1_000_000L; }
}