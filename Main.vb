Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On
Imports Console = System.Console
Imports Stopwatch = System.Diagnostics.Stopwatch

Public Module Main

    Dim stw As Stopwatch

    Public Sub Main()
        Try
            stw = new Stopwatch()

            stw.Start()
            Dim _stainedGlass As New StainedGlass()
            stw.Stop()

            ' do edit codes if you need

            CallCreate(_stainedGlass)

        Catch Ex As Exception
            Console.Error.WriteLine(ex)
        End Try
    End Sub

    Function CallCreate(_stainedGlass As StainedGlass) As Integer
        Dim H As Integer = CInt(Console.ReadLine())
        Dim _pixelsSize As Integer = CInt(Console.ReadLine()) - 1
        Dim pixels(_pixelsSize) As Integer
        For _idx As Integer = 0 To _pixelsSize
            pixels(_idx) = CInt(Console.ReadLine())
        Next _idx
        Dim N As Integer = CInt(Console.ReadLine())

        stw.Start()
        Dim _result As Integer() = _stainedGlass.create(H, pixels, N)
        stw.Stop()
        Console.Error.WriteLine("time: {0} ms", stw.ElapsedMilliseconds)

        Console.WriteLine(_result.Length)
        For Each _it As Integer In _result
            Console.WriteLine(_it)
        Next _it
        Console.Out.Flush()

        Return 0
    End Function

End Module