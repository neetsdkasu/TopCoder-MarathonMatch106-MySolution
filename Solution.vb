Option Explicit On
Option Strict   On
Option Compare  Binary
Option Infer    On

Imports System
Imports System.Collections.Generic

Imports Result = System.Tuple(Of Integer(), Double, Integer)

Public Class StainedGlass
    Dim startTime As Integer, limitTime As Integer
    Dim rand As New Random(19831983)

    Const VSize As Integer = 100
    Dim QSize As Integer = 5

    Dim H, W, N As Integer
    Dim flag(,) As Integer
    Dim field(,,) As Integer
    Dim nearest(,) As Integer
    Dim colors(,) As Dictionary(Of Integer, Integer)
    Dim pixels() As Integer

    Public Function create(H As Integer, pixels() As Integer, N As Integer) As Integer()
        startTime = Environment.TickCount
        limitTime = startTime + 19000
        Console.Error.WriteLine("{0}, {1}", limitTime, rand.Next(0, 100))

        Me.H = H
        Me.W = pixels.Length \ H
        Me.N = N
        Me.pixels = pixels

        Console.Error.WriteLine("H: {0}, W: {1}, N: {2}", H, W, N)

        Dim quantiFlag As Boolean = False
        Dim qhs As New HashSet(Of Integer)()
        For Each p As Integer In pixels
            qhs.Add(p)
            If qhs.Count > N \ 2 Then
                quantiFlag = True
                Exit For
            End If
        Next p
        qhs.Clear()
        qhs = Nothing

        ReDim field(VSize - 1, VSize - 1, 2)
        ReDim nearest(VSize - 1, VSize - 1)

        ReDim colors(N - 1, 2)
        For i As Integer = 0 To N - 1
            For k As Integer = 0 To 2
                colors(i, k) = New Dictionary(Of Integer, Integer)()
            Next k
        Next i

        Dim ret As Result = Generate(quantiFlag, False)
        Dim selr As Integer = QSize + 1

        For i As Integer = 1 To 5
            QSize += 1
            Dim tmp As Result = Generate(quantiFlag, False)
            If tmp.Item2 < ret.Item2 Then
                ret = tmp
                selr = QSize + 1
            End If
        Next i

        Dim remTime As Integer = limitTime - Environment.TickCount
        If remTime >= 3000 Then
            Dim tmp As Result = Generate(False, True)
            If tmp.Item2 < ret.Item2 Then
                ret = tmp
                selr = -1
            End If
        End If

        create = ret.Item1

        Console.Error.WriteLine("selr: {0}, qf: {1}, sc: {2}, cc: {3}, nn: {4}", selr, quantiFlag, ret.Item2, ret.Item3, ret.Item1.Length \ 3)

    End Function


    Private Function Generate(quantiFlag As Boolean, compressFlag As Boolean) As Result

        Dim retL As New List(Of Integer)()
        ReDim flag(VSize - 1, VSize - 1)

        ' Initialize Point Virtual Position
        For i As Integer = 1 To N
            Dim x As Integer = rand.Next(VSize)
            Dim y As Integer = rand.Next(VSize)
            Do While flag(y, x) > 0
                x = rand.Next(VSize)
                y = rand.Next(VSize)
            Loop
            flag(y, x) = 1
            retL.Add(y)
            retL.Add(x)
            retL.Add(0)
        Next i
        Dim ret() As Integer = retL.ToArray()
        retL.Clear()

        For i As Integer = 0 To N - 1
            For k As Integer = 0 To 2
                colors(i, k).Clear()
            Next k
        Next i

        ' Initialize Virtual Field Color And Nearest Point
        For row As Integer = 0 To VSize - 1
            Dim dh As Integer = H \ VSize
            Dim y As Integer = row * dh
            If row < H Mod VSize Then
                y += row
                dh += 1
            Else
                y += H Mod VSize
            End If
            For col As Integer = 0 To VSize - 1
                Dim dw As Integer = W \ VSize
                Dim x As Integer = col * dw
                If col < W Mod VSize Then
                    x += col
                    dw += 1
                Else
                    x += W Mod VSize
                End If
                Dim c As Integer = 0
                For k As Integer = 0 To 2
                    Dim es As New List(Of Integer)()
                    For dy As Integer = 0 To dh - 1
                        For dx As Integer = 0 To dw - 1
                            Dim e As Integer = pixels((y + dy) * W + (x + dx))
                            e = Factor(e, k)
                            If quantiFlag Then e = Quantization(e)
                            es.Add(e)
                        Next dx
                    Next dy
                    es.Sort()
                    Dim f As Integer = es(es.Count \ 2)
                    c = c Or (f << (k << 3))
                    field(row, col, k) = f
                Next k
                Dim minDist = Integer.MaxValue
                Dim seli = 0
                For i As Integer = 0 To N - 1
                    Dim dr As Integer = row - ret(i * 3)
                    Dim dc As Integer = col - ret(i * 3 + 1)
                    Dim dist As Integer = dr * dr + dc * dc
                    If dist < minDist Then
                        seli = i
                        minDist = dist
                    End If
                Next i
                nearest(row, col) = seli
                For k As Integer = 0 To 2
                    Dim e As Integer = Factor(c, k)
                    If colors(seli, k).ContainsKey(e) Then
                        colors(seli, k)(e) += 1
                    Else
                        colors(seli, k)(e) = 1
                    End IF
                Next k
            Next col
        Next row

        ' Initialize Each Point Color
        For i As Integer = 0 To N - 1
            Dim c As Integer = 0
            For k As Integer = 0 To 2
                Dim es As New List(Of Integer)()
                For Each ep As KeyValuePair(Of Integer, Integer) In colors(i, k)
                    For j As Integer = 1 To ep.Value
                        es.Add(ep.Key)
                    Next j
                Next ep
                es.Sort()
                c = c Or (es(es.Count \ 2) << (k << 3))
            Next k
            ret(i * 3 + 2) = c
        Next i

        Dim tmp(UBound(ret)) As Integer
        Array.Copy(ret, tmp, tmp.Length)

        Dim ptos As New List(Of Integer)()
        Dim pfrs As New List(Of Integer)()
        For cycle As Integer = 0 To 3000
            Dim ip As Integer = cycle Mod N
            Dim dy As Integer = rand.Next(5) - 2
            Dim dx As Integer = rand.Next(5) - 2
            Dim ny As Integer = Math.Max(0, Math.Min(VSize - 1, tmp(ip * 3) + dy))
            Dim nx As Integer = Math.Max(0, Math.Min(VSize - 1, tmp(ip * 3 + 1) + dx))
            Dim sc As Integer = 0
            Dim nc As Integer = tmp(ip * 3 + 2)
            ptos.Clear()
            pfrs.Clear()
            If nc < 0 Then
                ' appear random point
                Dim cl As Integer
                If flag(tmp(ip * 3), tmp(ip * 3 + 1)) = 1 AndAlso rand.Next(100) < 10 Then
                    ny = tmp(ip * 3)
                    nx = tmp(ip * 3 + 1)
                    cl = Not nc
                ElseIf flag(ny, nx) = 0 AndAlso rand.Next(100) < 10 Then
                    cl = Not nc
                Else
                    Do
                        ny = rand.Next(VSize)
                        nx = rand.Next(VSize)
                    Loop While flag(ny, nx) > 0
                    cl = (field(ny, nx, 2) << 16) Or (field(ny, nx, 1) << 8) Or field(ny, nx, 0)
                End If
                tmp(ip * 3 + 2) = cl
                sc = CalcAppearScore(tmp, ip, ny, nx, pfrs)
                tmp(ip * 3 + 2) = nc
                nc = cl
            ElseIf flag(ny, nx) > 0 Then
                ' disappear this point
                sc = CalcDisappearScore(tmp, ip, ptos)
                nc = Not nc
            Else
                ' move this point
                If ny = tmp(ip * 3) AndAlso nx = tmp(ip * 3 + 1) Then
                    For ty As Integer = Math.Max(0, tmp(ip * 3) - 2) To Math.Min(VSize - 1, tmp(ip * 3) + 2)
                        For tx As Integer = Math.Max(0, tmp(ip * 3 + 1) - 2) To Math.Min(VSize - 1, tmp(ip * 3 + 1) + 2)
                            If ny = ty AndAlso nx = tx Then Continue For
                            If ny = tmp(ip * 3) AndAlso nx = tmp(ip * 3 + 1) Then
                                ny = ty
                                nx = tx
                            ElseIf rand.Next(100) < 50 Then
                                ny = ty
                                nx = tx
                            End If
                        Next tx
                    Next ty
                End If
                sc = CalcMoveScore(tmp, ip, ny, nx, ptos, pfrs)
            End If

            If sc > 0 Then Continue For

            Dim oy As Integer = tmp(ip * 3)
            Dim ox As Integer = tmp(ip * 3 + 1)
            flag(oy, ox) -= 1
            flag(ny, nx) += 1
            tmp(ip * 3) = ny
            tmp(ip * 3 + 1) = nx
            tmp(ip * 3 + 2) = nc

            For i As Integer = 0 To ptos.Count - 1 Step 3
                Dim row As Integer = ptos(i)
                Dim col As Integer = ptos(i + 1)
                Dim sel As Integer = ptos(i + 2)
                nearest(row, col) = sel
            Next i

            For i As Integer = 0 To pfrs.Count - 1 Step 3
                Dim row As Integer = pfrs(i)
                Dim col As Integer = pfrs(i + 1)
                Dim old As Integer = pfrs(i + 2)
                nearest(row, col) = ip
            Next i

        Next cycle

        ret = tmp

        ' Convert To Real Position
        Dim idx(N - 1) As Integer
        retL.Clear()
        For i As Integer = 0 To N - 1
            If ret(i * 3 + 2) < 0 Then Continue For
            Dim y As Integer = ret(i * 3) * (H \ VSize)
            Dim x As Integer = ret(i * 3 + 1) * (W \ VSize)
            If ret(i * 3) < H Mod VSize Then
                y += ret(i * 3)
            Else
                y += H Mod VSize
            End If
            If ret(i * 3 + 1) < W Mod VSize Then
                x += ret(i * 3 + 1)
            Else
                x += W Mod VSize
            End If
            idx(i) = retL.Count \ 3
            retL.Add(y + (H \ VSize) \ 2)
            retL.Add(x + (W \ VSize) \ 2)
            retL.Add(ret(i * 3 + 2))
        Next i
        ret = retL.ToArray()

        Dim nn As Integer = ret.Length \ 3
        Dim ps(nn - 1) As List(Of Integer)
        For i As Integer = 0 To UBound(ps)
            ps(i) = New List(Of Integer)()
        Next i

        For row As Integer = 0 To VSize - 1
            Dim dh As Integer = H \ VSize
            Dim y As Integer = row * dh
            If row < H Mod VSize Then
                y += row
                dh += 1
            Else
                y += H Mod VSize
            End If
            For col As Integer = 0 To VSize - 1
                Dim dw As Integer = W \ VSize
                Dim x As Integer = col * dw
                If col < W Mod VSize Then
                    x += col
                    dw += 1
                Else
                    x += W Mod VSize
                End If
                Dim hs As New HashSet(Of Integer)()
                For tr As Integer = Math.Max(0, row - 3) To Math.Min(VSize - 1, row + 3)
                    For tc As Integer = Math.Max(0, col - 3) To Math.Min(VSize - 1, col + 3)
                        hs.Add(idx(nearest(tr, tc)))
                    Next tc
                Next tr
                Dim ts() As Integer = hs.ToArray()
                For ty As Integer = y To y + dh - 1
                    For tx As Integer = x To x + dw - 1
                        Dim minDist As Integer = Integer.MaxValue
                        Dim seli As Integer = 0
                        For Each i As Integer In ts
                            Dim dist As Integer = Distance2(ty, tx, ret(i * 3), ret(i * 3 + 1))
                            If dist < minDist Then
                                minDist = dist
                                seli = i
                            End If
                        Next i
                        ps(seli).Add(pixels(ty * W + tx))
                    Next tx
                Next ty
            Next col
        Next row

        Dim score As Integer = 0

        For i As Integer = 0 To nn - 1
            If ps(i).Count = 0 Then Continue For ' nazo bug
            Dim c As Integer = 0
            For k As Integer = 0 To 2
                Dim es As New List(Of Integer)()
                For Each e As Integer In ps(i)
                    Dim f As Integer = Factor(e, k)
                    If quantiFlag Then f = Quantization(f)
                    es.Add(f)
                Next e
                es.Sort()
                Dim ef As Integer = es(es.Count \ 2)
                c = c Or (ef << (k << 3))
                For Each e As Integer In ps(i)
                    score += Math.Abs(Factor(e, k) - ef)
                Next e
            Next k
            ret(i * 3 + 2) = c
        Next i

        If compressFlag Then
            For i As Integer = 0 To nn - 1
                Dim c As Integer = ret(i * 3 + 2)
                Dim selj As Integer = i
                Dim seld As Integer = Integer.MaxValue
                For j As Integer = 0 To nn - 1
                    If i = j Then Continue For
                    Dim e As Integer = ret(j * 3 + 2)
                    Dim d As Integer = 0
                    For k As Integer = 0 To 2
                        Dim f As Integer = Factor(e, k) - Factor(c, k)
                        d += f * f
                    Next k
                    If d < seld Then
                        selj = j
                        seld = d
                    End If
                Next j
                ret(i * 3 + 2) = ret(selj * 3 + 2)
            Next i
        End If

        Dim shs As New HashSet(Of Integer)()
        For i As Integer = 0 To nn - 1
            shs.Add(ret(i * 3 + 2))
        Next i

        Dim dScore As Double = CDbl(score) * ((1.0 + CDbl(shs.Count) / CDbl(N)) ^ 2.0)

        Generate = New Result(ret, dScore, shs.Count)

    End Function

    Private Function Quantization(f As Integer) As Integer
        Quantization = ((QSize * f + 128) \ 255) * 255 \ QSize
    End Function

    Private Function Factor(c As Integer, k As Integer) As Integer
        Factor = (c >> (k << 3)) And &HFF
    End Function

    Private Function Distance2(y1 As Integer, x1 As Integer, y2 As Integer, x2 As Integer) As Integer
        Dim dy As Integer = y1 - y2
        Dim dx As Integer = x1 - x2
        Distance2 = dy * dy + dx * dx
    End Function

    Private Function CalcMoveScore(tmp() As Integer, _
            ip As Integer, ny As Integer, nx As Integer, _
            ptos As List(Of Integer), pfrs As List(Of Integer)) As Integer

        Dim sc As Integer = 0

        For row As Integer = 0 To VSize - 1
            For col As Integer = 0 To VSize - 1
                Dim bp As Integer = nearest(row, col)
                If ip = bp Then
                    Dim minDist As Integer = Integer.MaxValue
                    Dim selj As Integer = -1
                    For j As Integer = 0 To N - 1
                        If tmp(j * 3 + 2) < 0 Then Continue For
                        Dim dist As Integer
                        If j = ip Then
                            dist = Distance2(row, col, ny, nx)
                        Else
                            dist = Distance2(row, col, tmp(j * 3), tmp(j * 3 + 1))
                        End If
                        If dist < minDist Then
                            minDist = dist
                            selj = j
                        End If
                    Next j
                    If selj < 0 Then
                        CalcMoveScore = Integer.MaxValue
                        Exit Function
                    End If
                    If selj <> ip Then
                        ' Change Nearest
                        ptos.Add(row)
                        ptos.Add(col)
                        ptos.Add(selj)
                        For k As Integer = 0 To 2
                            Dim e As Integer = field(row, col, k)
                            sc += Math.Abs(e - Factor(tmp(selj * 3 + 2), k))
                            sc -= Math.Abs(e - Factor(tmp(ip * 3 + 2), k))
                        Next k
                    End If
                Else
                    Dim bDist As Integer = Distance2(row, col, tmp(bp * 3), tmp(bp * 3 + 1))
                    Dim nDist As Integer = Distance2(row, col, ny, nx)
                    If nDist < bDist Then
                        ' Change Nearest
                        pfrs.Add(row)
                        pfrs.Add(col)
                        pfrs.Add(bp)
                        For k As Integer = 0 To 2
                            Dim e As Integer = field(row, col, k)
                            sc -= Math.Abs(e - Factor(tmp(bp * 3 + 2), k))
                            sc += Math.Abs(e - Factor(tmp(ip * 3 + 2), k))
                        Next k
                    End If
                End If
            Next col
        Next row

        CalcMoveScore = sc
    End Function

    Private Function CalcDisappearScore(tmp() As Integer, _
            ip As Integer, ptos As List(Of Integer)) As Integer

        Dim sc As Integer = 0

        For row As Integer = 0 To VSize - 1
            For col As Integer = 0 To VSize - 1
                Dim bp As Integer = nearest(row, col)
                If ip <> bp Then Continue For
                Dim minDist As Integer = Integer.MaxValue
                Dim selj As Integer = -1
                For j As Integer = 0 To N - 1
                    If j = ip OrElse tmp(j * 3 + 2) < 0 Then Continue For
                    Dim dist As Integer = Distance2(row, col, tmp(j * 3), tmp(j * 3 + 1))
                    If dist < minDist Then
                        minDist = dist
                        selj = j
                    End If
                Next j
                If selj < 0 Then
                    CalcDisappearScore = Integer.MaxValue
                    Exit Function
                End If
                If selj <> ip Then
                    ' Change Nearest
                    ptos.Add(row)
                    ptos.Add(col)
                    ptos.Add(selj)
                    For k As Integer = 0 To 2
                        Dim e As Integer = field(row, col, k)
                        sc += Math.Abs(e - Factor(tmp(selj * 3 + 2), k))
                        sc -= Math.Abs(e - Factor(tmp(ip * 3 + 2), k))
                    Next k
                End If
            Next col
        Next row

        CalcDisappearScore = sc
    End Function


    Private Function CalcAppearScore(tmp() As Integer, _
            ip As Integer, ny As Integer, nx As Integer, _
            pfrs As List(Of Integer)) As Integer

        Dim sc As Integer = 0

        For row As Integer = 0 To VSize - 1
            For col As Integer = 0 To VSize - 1
                Dim bp As Integer = nearest(row, col)
                Dim bDist As Integer = Distance2(row, col, tmp(bp * 3), tmp(bp * 3 + 1))
                Dim nDist As Integer = Distance2(row, col, ny, nx)
                If nDist < bDist Then
                    ' Change Nearest
                    pfrs.Add(row)
                    pfrs.Add(col)
                    pfrs.Add(bp)
                    For k As Integer = 0 To 2
                        Dim e As Integer = field(row, col, k)
                        sc -= Math.Abs(e - Factor(tmp(bp * 3 + 2), k))
                        sc += Math.Abs(e - Factor(tmp(ip * 3 + 2), k))
                    Next k
                End If
            Next col
        Next row

        CalcAppearScore = sc
    End Function
End Class